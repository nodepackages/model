/**
 * Created by tomislavfabeta on 17/09/16.
 */
module.exports = Model;
var _ = require("underscore");
var ERRORS = {
    VALIDATION_FAIL: 'Validation failed'
};

/**
 * TODO validate.js npm package (validating model params)
 */
function Model(){
    this.getColumns = function(){return []};
    this.timestamps = {
        created_at: false,
        updated_at: false,
    };
    this.errors = [];
}

Model.prototype = {
    constructor: Model,

    before: function(){
        this.databaseFunctions.table = this.table;
        this.databaseFunctions.timestamps = this.timestamps;
        this.databaseFunctions.getColumns = this.getColumns;

    },

    getParams: function (key) {
        var columns = this.getColumns();
        var self = this;

        if (key){
            if (columns.indexOf(key) >= 0){
                return self[key];
            }
        }else{
            var params = {};

            for (var key in columns){
                var column = columns[key];
                if (self[column]){
                    params[column] = self[column];
                }
            }

            return params;
        }
    },

    save: function (done) {
        if (!this.validate(done)) return false;

        this.before();
        console.log("Active Record Save", this.getParams(), this.errors);
        _save.bind(this)(done);
    },

    update: function (done) {
        if (!this.validate(done)) return false;

        this.before();
        _update.bind(this)(done);
    },

    get: function(done){
        if (!this.validate(done)) return false;

        this.before();
        _get.bind(this)(done);
    },

    find: function (done) {
        if (!this.validate(done)) return false;

        this.before();
        _find.bind(this)(done);
    },

    remove: function (done) {
        if (!this.validate(done)) return false;

        this.before();
        _remove.bind(this)(done);
    },

    rules: rules,
    validate: validate,
    load: load,
};

/**
 * Public Functions
 */

function rules(){
    return {};
}

/**
 * Clean params with rules
 * @param key
 * @param value
 * @returns {*}
 */

function load(params){
    _load.bind(this)(params);
}

function validate(done){
    if (this.errors.length > 0){
        done(this.errors);
        return false;
    }

    return true;
}


/**
 * Private functions
 */
function pushToError(type, data){

    if (!this.errors){
        this.errors = [];
    }

    switch (type){
        case 'param':
            this.errors.push('Param ' + data.param + ' should be ' + data.type);
            break;
    }

    return false;
}

function customRules() {
    return {
        'string': function (value) {
            if (typeof value == 'string') return true;
            return false;
        },
        'int': function (value) {
            if (typeof value != 'string' && typeof value != 'number') return false;
            if (/^\d+$/.test(value)) return true;
            return false;
        },
        'object': function (value) {
            if (typeof value == 'object') return true;
            if (typeof value != 'string' && typeof value != 'object') return false;
            try {
                value = JSON.parse(value);
            } catch (err) {
                return false;
            }
        }
    }
}

function cleanParam(key, value){
    var rules = this.rules();
    var self = this;

    for (var type in rules) {
        var ruleParams = rules[type];

        if (ruleParams.indexOf(key) >= 0){
            value = (function(type, value){
                switch (type){
                    case 'string':
                        if (!self.customRules.string(value)) return pushToError.bind(self)('param', {param:key, type:type});
                        return '' + value;
                        break;

                    case 'int':
                    case 'integer':
                        if (!self.customRules.int(value)) return pushToError.bind(self)('param', {param:key, type:type});
                        return parseInt(value);
                        break;

                    case 'object':
                        if (!self.customRules.object(value)) return pushToError.bind(self)('param', {param:key, type:type});
                        if (typeof value == 'object') return value;
                        return JSON.parse(value);
                }

            })(type, value);
        }
    }

    return value;
}

/**
 * TODO Remove validation from _load and put in validate
 */
function _load(params){
    this.customRules =  _.extend(customRules(), this.customRules || {});

    var self = this;
    var columns = self.getColumns();

    for (var key in columns){
        self[columns[key]] = null;
    }

    if (params[this.table]){
        var myParams = params[self.table];
        for (var key in myParams){
            var paramValue = myParams[key];
            if (columns.indexOf(key) >= 0){
                var value = cleanParam.bind(this)(key, paramValue);

                self[key] = value;

            }
        }
    }
}


function _save(done){
    this.databaseFunctions.save(this.getParams(), done);
}

function _get(done){
    this.databaseFunctions.get(this.getParams("id"), done);

}
function _update(done){
    this.databaseFunctions.update(this.getParams(), done);

}
function _remove(done){
    this.databaseFunctions.remove(this.getParams(), done);
}
function _find(done){
    this.databaseFunctions.find(this.getParams(), done);
}
