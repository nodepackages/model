import {BaseModel} from "../../base/BaseModel";
import {ConditionInterface} from "../ConditionInterface";
/**
 * Created by tomislavfabeta on 8/1/17.
 */

export interface BaseModelInterface {

    /**
     * @param condition
     * @return QueryInterface
     */
    find(condition: ConditionInterface): {};

    /**
     * @param condition
     * @return BaseModel
     */
    findOne(condition: ConditionInterface): BaseModel;

    /**
     * @param condition
     * @return BaseModel
     */
    findAll(condition: ConditionInterface): BaseModel;

    /**
     * @param runValidation
     * @param attributeNames
     * @return boolean
     */
    save(runValidation: boolean, attributeNames: Array<string>): boolean;

    /**
     * @param runValidation
     * @param attributeNames
     * @return boolean
     */
    insert(runValidation: boolean, attributeNames: Array<string>): boolean;

}