/**
 * Created by tomislavfabeta on 12/1/17.
 */
export interface BaseValidatorInterface {

    /**
     * check if the string contains the seed
     * @param str
     * @param seed
     * @return {boolean}
     */
    contains(str: string, seed: string): boolean;
    /**
     * check if the string matches the comparison
     * @param str
     * @param comparison
     * @return {boolean}
     */
    equals(str: string, comparison: string): boolean;
    /**
     * check if the string is a date that's after the specified date (defaults to now)
     * @param str
     * @param date
     * @return {boolean}
     */
    isAfter(str: string, date: Date): boolean;
    /**
     * check if the string is a date that's before the specified date
     * @param str
     * @param date
     * @return {boolean}
     */
    isBefore(str: string, date: Date): boolean;
    /**
     * check if a string is a boolean
     * @param str
     * @return {boolean}
     */
    isBoolean(str: string): boolean;
    /**
     * check if the string is a credit card
     * @param str
     * @return {boolean}
     */
    isCreditCard(str: string): boolean;
    /**
     * check if the string is a valid currency amount. options is an object which defaults to {symbol: '$', require_symbol: false, allow_space_after_symbol: false, symbol_after_digits: false, allow_negatives: true, parens_for_negatives: false, negative_sign_before_digits: false, negative_sign_after_digits: false, allow_negative_sign_placeholder: false, thousands_separator: ',', decimal_separator: '.', allow_space_after_digits: false }
     * @param str
     * @param options
     * @return {boolean}
     */
    isCurrency(str: string, options: any): boolean;
    /**
     * check if the string is a date
     * @param str
     * @return {boolean}
     */
    isDate(str: string): boolean;
    /**
     * check if the string represents a decimal number, such as 0.1, .3, 1.1, 1.00003, 4.0, etc
     * @param str
     * @return {boolean}
     */
    isDecimal(str: string): boolean;
    // isEmail(str:string, options:IsEmailOptionsInterface):boolean
    /**
     * check if the string has a length of zero
     * @param str
     * @return {boolean}
     */
    isEmpty(str: string): boolean;
    /**
     * check if the string is a float. options is an object which can contain the keys min, max, gt, and/or lt to validate the float is within boundaries (e.g. { min: 7.22, max: 9.55 }). min and max are equivalent to 'greater or equal' and 'less or equal', respectively while gt and lt are their strict counterparts
     * @param str
     * @param options
     * @return {boolean}
     */
    isFloat(str: string, options: IsNumberOptionsInterface): boolean;
    /**
     * check if the string is an integer. options is an object which can contain the keys min and/or max to check the integer is within boundaries (e.g. { min: 10, max: 99 }). options can also contain the key allow_leading_zeroes, which when set to false will disallow integer values with leading zeroes (e.g. { allow_leading_zeroes: false }). Finally, options can contain the keys gt and/or lt which will enforce integers being greater than or less than, respectively, the value provided (e.g. {gt: 1, lt: 4} for a number between 1 and 4)
     * @param str
     * @param options
     * @return {boolean}
     */
    isInt(str: string, options: IsNumberOptionsInterface): boolean;
    /**
     * check if the string is valid JSON (note: uses JSON.parse)
     * @param str
     * @return {boolean}
     */
    isJSON(str: string): boolean;
    /**
     * check if the string's length falls in a range. options is an object which defaults to {min:0, max: undefined}. Note: this function takes into account surrogate pairs
     * @param str
     * @param options
     * @return {boolean}
     */
    isLength(str: string, options: IsLengthInterface): boolean;
    /**
     * check if the string contains only numbers
     * @param str
     * @return {boolean}
     */
    isNumeric(str: string): boolean;
    /**
     * checks characters if they appear in the whitelist
     * @param str
     * @param chars
     * @return {boolean}
     */
    isWhitelisted(str: string, chars: string): boolean;

}

export interface IsNumberOptionsInterface {
    min: number;
    max: number;
    gt: number;
    lt: number;
}

export interface IsLengthInterface {
    min: number;
    max?: number;
}

export interface ValidationOptionsInterface {
    value: any;
    min?: number;
    max?: number;
    gt?: number;
}