/**
 * Created by tomislavfabeta on 9/1/17.
 */

export interface BaseConditionInterface {
    params: Object;
    limit: number;
    offset: number;
    index: string;
    order: string;
}