/**
 * Created by tomislavfabeta on 8/1/17.
 */

export abstract class BaseModel extends Component {

    static SCENARIO_DEFAULT: string = `default`;
    static EVENT_BEFORE_VALIDATE: string = `beforeValidate`;
    static EVENT_AFTER_VALIDATE: string = `afterValidate`;

    private _errors: any;
    private _validators: any;
    private _scenario: string = BaseModel.SCENARIO_DEFAULT;

    attributes: any = {};

    constructor () {
        super();
    }

    /**
     *
     * @returns {Array}
     */
    rules(): any {
        return [];
    };
    scenarios(): any {
        let scenarios: any = {};
        scenarios[BaseModel.SCENARIO_DEFAULT] = [];
    }

    validate(attributeName: Array<string> = [], clearErrors: boolean = true): boolean {
        if (clearErrors) {
            this.clearErrors();
        }

        if (!this.beforeValidate()) {
            return false;
        }

        /**
         * Scenarios not used
         * TODO Add scenarios
         */
        let scenarios = this.scenarios();
        let scenario = this.getScenario();


        //
        // $scenarios = $this->scenarios();
        // $scenario = $this->getScenario();
        // if (!isset($scenarios[$scenario])) {
        //     throw new InvalidParamException("Unknown scenario: $scenario");
        // }
        //
        // if ($attributeNames === null) {
        //     $attributeNames = $this->activeAttributes();
        // }
        //
        // foreach ($this->getActiveValidators() as $validator) {
        //     $validator->validateAttributes($this, $attributeNames);
        // }
        // $this->afterValidate();
        //
        // return !$this->hasErrors();
    }

    /**
     * Removes errors for all attributes or a single attribute.
     * @param {string} attribute attribute name. Use null to remove errors for all attribute.
     */
    clearErrors(attribute: string = null): void {
        if (attribute === null) {
            this._errors = {};
        } else {
            this._errors[attribute] = [];
        }
    }

    /**
     * Returns the attribute labels.
     *
     * Attribute labels are mainly used for display purpose. For example, given an attribute
     * `firstName`, we can declare a label `First Name` which is more user-friendly and can
     * be displayed to end users.
     *
     * By default an attribute label is generated using [[generateAttributeLabel()]].
     * This method allows you to explicitly specify attribute labels.
     *
     * @returns {Array}
     * @see generateAttributeLabel()
     */
    static attributeLabels(): Array<string> {
        return [];
    }

    /**
     * Returns the attribute hints.
     *
     * Attribute hints are mainly used for display purpose. For example, given an attribute
     * `isPublic`, we can declare a hint `Whether the post should be visible for not logged in users`,
     * which provides user-friendly description of the attribute meaning and can be displayed to end users.
     *
     * Unlike label hint will not be generated, if its explicit declaration is omitted.
     *
     * Note, in order to inherit hints defined in the parent class, a child class needs to
     * merge the parent hints with child hints using functions such as `concat()`.
     *
     * @return object attribute hints (name => hint)
     */
    attributeHints(): Object {
        return {};
    }

    generateAttributeLabel(name: string): string {
        return name;
        // return Inflector::camel2words($name, true);
    }

    /**
     * Returns attribute values.
     * @param {Array} names list of attributes whose value needs to be returned.
     * Defaults to null, meaning all attributes listed in [[attributes()]] will be returned.
     * If it is an array, only the attributes in the array will be returned.
     * @param {Array} except list of attributes whose value should NOT be returned.
     * @return object attribute values (name => value).
     */
    getAttributes(names: Array<string> = null, except: Array<string> = []): any {
        let values: any = {};

        if (names === null) {
            names = this.attributes;
        }

        names.forEach(function (element) {
            values[element] = this.attributes[element];
        });

        except.forEach(function (element) {
            delete values[element];
        });

        return values;
    }

    /**
     * Sets the attribute values in a massive way.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     * @param values attribute values (name => value) to be assigned to the model.
     * @param safeOnly safeOnly whether the assignments should only be done to the safe attributes.
     */
    setAttributes(values: any, safeOnly: boolean = true) {
        /**
         * If array
         */
        if (Object.prototype.toString.call( values ) === '[object Array]') {
            let attributes = array_flip(safeOnly ? this.safeAttributes() : this.attributes);
        }
        // if (is_array($values)) {
        //     $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes());
        //     foreach ($values as $name => $value) {
        //         if (isset($attributes[$name])) {
        //             $this->$name = $value;
        //         } elseif ($safeOnly) {
        //             $this->onUnsafeAttribute($name, $value);
        //         }
        //     }
        // }
    }

    /**
     * Returns the attribute names that are subject to validation in the current scenario.
     * @return string[] safe attribute names
     */
    safeAttributes(): string[] {
        let scenario = this.getScenario();
        let scenarios = this.scenarios();

        if (!scenarios[scenario]) {
            return [];
        }

        let attributes: string[] = scenarios[scenario];

        attributes.forEach(function (element) {
            let attribute = element;

            if (attribute[0] === '!') {
                element = attribute.substr(1);
            }
        });

        return attributes;
    }

    /**
     * This method is invoked before validation starts.
     * The default implementation raises a `beforeValidate` event.
     * You may override this method to do preliminary checks before validation.
     * Make sure the parent implementation is invoked so that the event can be raised.
     * @return boolean whether the validation should be executed. Defaults to true.
     * If false is returned, the validation will stop and the model is considered invalid.
     */
    beforeValidate(): boolean {
        return true;
        // $event = new ModelEvent;
        // $this->trigger(self::EVENT_BEFORE_VALIDATE, $event);
        //
        // return $event->isValid;
    }

    getScenario() {
        return this._scenario;
    }

    setScenario(value: string) {
        this._scenario = value;
    }
}

function array_flip( trans: Array<any> ) {
    let tmp_ar: any = {};

    for ( let key in trans ) {
        if ( trans.hasOwnProperty( key ) ) {
            tmp_ar[trans[key]] = key;
        }
    }

    return tmp_ar;
}