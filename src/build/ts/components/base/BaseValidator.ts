///<reference path="../interfaces/ValidatorInterface.ts"/>
///<reference path="../interfaces/base/BaseValidatorTyped.ts"/>
import {ValidatorInterface} from "../interfaces/ValidatorInterface";
// import {IsNumberOptionsInterface, IsLengthInterface} from "../interfaces/base/BaseValidatorInterface";
// declare function require(name: string): any;
// let validator = require('validator');

/**
 * Created by tomislavfabeta on 10/1/17.
 */

export class BaseValidator implements ValidatorInterface {
    version: string;

    isAfter(str: string, date?: string): boolean {
        return validator.isAfter(str, date);
    }

    isBefore(str: string, date?: string): boolean {
        return validator.isBefore(str, date);
    }

    escape(input: string): string {
        return validator.escape(input);
    }

    contains(str: string, elem: any): boolean {
        return validator.contains(str, elem);
    }

    equals(str: string, comparison: string): boolean {
        return validator.equals(str, comparison);
    }

    isAlpha(str: string, locale?: ValidatorJS.AlphaLocale): boolean {
        return validator.isAlpha(str, locale);
    }

    isAlphanumeric(str: string, locale?: ValidatorJS.AlphanumericLocale): boolean {
        return validator.isAlphanumeric(str, locale);
    }

    isAscii(str: string): boolean {
        return validator.isAscii(str);
    }

    isBase64(str: string): boolean {
        return validator.isBase64(str);
    }

    isBoolean(str: string): boolean {
        return validator.isBoolean(str);
    }

    isByteLength(str: string, options: ValidatorJS.IsByteLengthOptions): boolean;
    isByteLength(str: string, min: number, max?: number): boolean;
    isByteLength(str: string, options: any, max?: number): boolean {
        return validator.isByteLength(str, options, max);
    }

    isCreditCard(str: string): boolean {
        return validator.isCreditCard(str);
    }

    isCurrency(str: string, options?: ValidatorJS.IsCurrencyOptions): boolean {
        return validator.isCurrency(str, options);
    }

    isDataURI(str: string): boolean {
        return validator.isDataURI(str);
    }

    isDate(str: string): boolean {
        return validator.isDate(str);
    }

    isDecimal(str: string): boolean {
        return validator.isDecimal(str);
    }

    isDivisibleBy(str: string, num: number): boolean {
        return validator.isDivisibleBy(str, num);
    }

    isEmail(str: string, options?: ValidatorJS.IsEmailOptions): boolean {
        return validator.isEmail(str, options);
    }

    isEmpty(str: string): boolean {
        return validator.isEmpty(str);
    }

    isFQDN(str: string, options?: ValidatorJS.IsFQDNOptions): boolean {
        return validator.isFQDN(str, options);
    }

    isFloat(str: string, options?: ValidatorJS.IsFloatOptions): boolean {
        return validator.isFloat(str, options);
    }

    isFullWidth(str: string): boolean {
        return validator.isFullWidth(str);
    }

    isHalfWidth(str: string): boolean {
        return validator.isHalfWidth(str);
    }

    isHexColor(str: string): boolean {
        return validator.isHexColor(str);
    }

    isHexadecimal(str: string): boolean {
        return validator.isHexadecimal(str);
    }

    isIP(str: string, version?: number): boolean {
        return validator.isIP(str, version);
    }

    isISBN(str: string, version?: number): boolean {
        return validator.isISBN(str, version);
    }

    isISIN(str: string): boolean {
        return validator.isISIN(str);
    }

    isISO8601(str: string): boolean {
        return validator.isISO8601(str);
    }

    isIn(str: string, values: any[]): boolean {
        return validator.isIn(str, values);
    }

    isInt(str: string, options?: ValidatorJS.IsIntOptions): boolean {
        return validator.isInt(str, options);
    }

    isJSON(str: string): boolean {
        return validator.isJSON(str);
    }

    isLength(str: string, options: ValidatorJS.IsLengthOptions): boolean;
    isLength(str: string, min: number, max?: number): boolean;
    isLength(str: string, options: any, max?: number): boolean {
        return validator.isLength(str, options);
    }

    isLowercase(str: string): boolean {
        return validator.isLowercase(str);
    }

    isMACAddress(str: string): boolean {
        return validator.isMACAddress(str);
    }

    isMD5(str: string): boolean {
        return validator.isMD5(str);
    }

    isMobilePhone(str: string, locale: ValidatorJS.MobilePhoneLocale): boolean {
        return validator.isMobilePhone(str, locale);
    }

    isMongoId(str: string): boolean {
        return validator.isMongoId(str);
    }

    isMultibyte(str: string): boolean {
        return validator.isMultibyte(str);
    }

    isNull(str: string): boolean {
        return validator.isNull(str);
    }

    isNumeric(str: string): boolean {
        return validator.isNumeric(str);
    }

    isSurrogatePair(str: string): boolean {
        return validator.isSurrogatePair(str);
    }

    isURL(str: string, options?: ValidatorJS.IsURLOptions): boolean {
        return validator.isURL(str, options);
    }

    isUUID(str: string, version?: string|number): boolean {
        return validator.isUUID(str);
    }

    isUppercase(str: string): boolean {
        return validator.isUppercase(str);
    }

    isVariableWidth(str: string): boolean {
        return validator.isVariableWidth(str);
    }

    isWhitelisted(str: string, chars: string|string[]): boolean {
        return validator.isWhitelisted(str, chars);
    }

    matches(str: string, pattern: RegExp|string, modifiers?: string): boolean {
        return validator.matches(str, pattern);
    }

    blacklist(input: string, chars: string): string {
        return validator.blacklist(input, chars);
    }

    static escape(input: string): string {
        return validator.escape(input);
    }

    unescape(input: string): string {
        return validator.unescape(input);
    }

    ltrim(input: string, chars?: string): string {
        return validator.ltrim(input, chars);
    }

    normalizeEmail(email: string, options?: ValidatorJS.NormalizeEmailOptions): string|any {
        return validator.normalizeEmail(email, options);
    }

    rtrim(input: string, chars?: string): string {
        return validator.rtrim(input, chars);
    }

    stripLow(input: string, keep_new_lines?: boolean): string {
        return validator.stripLow(input, keep_new_lines);
    }

    toBoolean(input: string, strict?: boolean): boolean {
        return validator.toBoolean(input, strict);
    }

    toDate(input: string): Date {
        return validator.toDate(input);
    }

    toFloat(input: string): number {
        return validator.toFloat(input);
    }

    toInt(input: string, radix?: number): number {
        return validator.toInt(input, radix);
    }

    trim(input: string, chars?: string): string {
        return validator.trim(input, chars);
    }

    whitelist(input: string, chars: string): string {
        return validator.whitelist(input, chars);
    }

    extend<T extends Function>(name: string, fn: T): void {
    }
    // /**
    //  * check if the string contains the seed
    //  * @param str
    //  * @param seed
    //  * @return {boolean}
    //  */
    // contains(str: string, seed: string): boolean {
    //     return validator.contains(str, seed);
    // }
    //
    // /**
    //  * check if the string matches the comparison
    //  * @param str
    //  * @param comparison
    //  * @return {boolean}
    //  */
    // equals(str: string, comparison: string): boolean {
    //     return validator.equals(str, comparison);
    // }
    //
    // /**
    //  * check if the string is a date that's after the specified date (defaults to now)
    //  * @param str
    //  * @param date
    //  * @return {boolean}
    //  */
    // isAfter(str: string, date: Date): boolean {
    //     return validator.isAfter(str, date);
    // }
    //
    // /**
    //  * check if the string is a date that's before the specified date
    //  * @param str
    //  * @param date
    //  * @return {boolean}
    //  */
    // isBefore(str: string, date: Date): boolean {
    //     return validator.isBefore(str, date);
    // }
    //
    // /**
    //  * check if a string is a boolean
    //  * @param str
    //  * @return {boolean}
    //  */
    // isBoolean(str: string): boolean {
    //     return validator.isBoolean(str);
    // }
    //
    // /**
    //  * check if the string is a credit card
    //  * @param str
    //  * @return {boolean}
    //  */
    // isCreditCard(str: string): boolean {
    //     return validator.isCreditCard(str);
    // }
    //
    // /**
    //  * check if the string is a valid currency amount. options is an object which defaults to {symbol: '$', require_symbol: false, allow_space_after_symbol: false, symbol_after_digits: false, allow_negatives: true, parens_for_negatives: false, negative_sign_before_digits: false, negative_sign_after_digits: false, allow_negative_sign_placeholder: false, thousands_separator: ',', decimal_separator: '.', allow_space_after_digits: false }
    //  * @param str
    //  * @param options
    //  * @return {boolean}
    //  */
    // isCurrency(str: string, options: any): boolean {
    //     return validator.isCurrency(str, options);
    // }
    //
    // /**
    //  * check if the string is a date
    //  * @param str
    //  * @return {boolean}
    //  */
    // isDate(str: string): boolean {
    //     return validator.isDate(str);
    // }
    //
    // /**
    //  * check if the string represents a decimal number, such as 0.1, .3, 1.1, 1.00003, 4.0, etc
    //  * @param str
    //  * @return {boolean}
    //  */
    // isDecimal(str: string): boolean {
    //     return validator.isDecimal(str);
    // }
    //
    // /**
    //  * check if the string has a length of zero
    //  * @param str
    //  * @return {boolean}
    //  */
    // isEmpty(str: string): boolean {
    //     return validator.isDecimal(str);
    // }
    //
    // /**
    //  * check if the string is a float. options is an object which can contain the keys min, max, gt, and/or lt to validate the float is within boundaries (e.g. { min: 7.22, max: 9.55 }). min and max are equivalent to 'greater or equal' and 'less or equal', respectively while gt and lt are their strict counterparts
    //  * @param str
    //  * @param options
    //  * @return {boolean}
    //  */
    // isFloat(str: string, options: IsNumberOptionsInterface): boolean {
    //     return validator.isFloat(str, options);
    // }
    //
    // /**
    //  * check if the string is an integer. options is an object which can contain the keys min and/or max to check the integer is within boundaries (e.g. { min: 10, max: 99 }). options can also contain the key allow_leading_zeroes, which when set to false will disallow integer values with leading zeroes (e.g. { allow_leading_zeroes: false }). Finally, options can contain the keys gt and/or lt which will enforce integers being greater than or less than, respectively, the value provided (e.g. {gt: 1, lt: 4} for a number between 1 and 4)
    //  * @param str
    //  * @param options
    //  * @return {boolean}
    //  */
    // isInt(str: string, options: IsNumberOptionsInterface): boolean {
    //     return validator.isInt(str, options);
    // }
    //
    // /**
    //  * check if the string is valid JSON (note: uses JSON.parse)
    //  * @param str
    //  * @return {boolean}
    //  */
    // isJSON(str: string): boolean {
    //     return validator.isJSON(str);
    // }
    //
    // /**
    //  * check if the string's length falls in a range. options is an object which defaults to {min:0, max: undefined}. Note: this function takes into account surrogate pairs
    //  * @param str
    //  * @param options
    //  * @return {boolean}
    //  */
    // isLength(str: string, options: IsLengthInterface): boolean {
    //     return validator.isLength(str, options);
    // }
    //
    // /**
    //  * check if the string contains only numbers
    //  * @param str
    //  * @return {boolean}
    //  */
    // isNumeric(str: string): boolean {
    //     return validator.isNumeric(str);
    // }
    //
    // /**
    //  * checks characters if they appear in the whitelist
    //  * @param str
    //  * @param chars
    //  * @return {boolean}
    //  */
    // isWhitelisted(str: string, chars: string): boolean {
    //     return validator.isWhitelisted(str, chars);
    // }

    constructor() {

    }

}