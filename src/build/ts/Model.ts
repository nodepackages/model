import {BaseModel} from "./components/base/BaseModel";
import {BaseModelInterface} from "./components/interfaces/base/BaseModelInterface";
import {ConditionInterface} from "./components/interfaces/ConditionInterface";
/**
 * Created by tomislavfabeta on 5/1/17.
 */


export class Model extends BaseModel implements BaseModelInterface{
    find(condition: ConditionInterface): {} {
        return undefined;
    }

    findOne(condition: ConditionInterface): BaseModel {
        return undefined;
    }

    findAll(condition: ConditionInterface): BaseModel {
        return undefined;
    }

    save(runValidation: boolean, attributeNames: Array<string>): boolean {
        return undefined;
    }

    insert(runValidation: boolean, attributeNames: Array<string>): boolean {
        return undefined;
    }

}